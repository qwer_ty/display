/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/15 09:06:41 by areheis           #+#    #+#             */
/*   Updated: 2020/09/16 09:33:56 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	int				a;
	int				b;
	unsigned int	i;

	a = 0;
	i = -1;
	b = 0;
	while (dest[a] != '\0')
	{
		a++;
	}
	while (++i < nb && src[b] != '\0')
	{
		dest[b + a] = src[b];
		b++;
	}
	dest[b + a] = '\0';
	return (dest);
}
