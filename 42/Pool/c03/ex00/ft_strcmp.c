  /* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/15 12:36:36 by areheis           #+#    #+#             */
/*   Updated: 2020/09/16 09:04:25 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (*s1 == *s2)
	{
		if ((s1[i] && s2[i]) == '\0')
		{
			return (0);
		}
		s1++;
		s2++;
		i++;
	}
	return (*s1 - *s2);
}
