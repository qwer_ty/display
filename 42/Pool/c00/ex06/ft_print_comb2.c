/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/10 11:54:57 by areheis           #+#    #+#             */
/*   Updated: 2020/09/10 17:05:47 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char a)
{
	write(1, &a, 1);
}

void	ft_print_comb2(void)
{
	int a;
	int b;

	a = 0;
	b = 1;
	while (a < 99)
	{
		ft_putchar(a / 10 + '0');
		ft_putchar(a % 10 + '0');
		write(1, " ", 1);
		ft_putchar(b / 10 + '0');
		ft_putchar(b % 10 + '0');
		if (++b > 99)
		{
			b = ++a + 1;
		}
		if (a < 99)
		{
			write(1, ", ", 2);
		}
	}
}
