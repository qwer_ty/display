/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/10 13:30:53 by areheis           #+#    #+#             */
/*   Updated: 2020/09/11 14:25:33 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	long nbout;

	nbout = (long)nb;
	if (nbout >= -9 && nbout <= -1)
	{
		ft_putchar('-');
		nbout = nbout * -1;
		ft_putchar(nbout + '0');
	}
	else if (nbout <= -10)
	{
		ft_putchar('-');
		nbout = nbout * -1;
		ft_putnbr(nbout / 10);
		ft_putchar(nbout % 10 + '0');
	}
	else if (nbout >= 0 && nbout <= 9)
	{
		ft_putchar(nbout + '0');
	}
	else if (nbout >= 10)
	{
		ft_putnbr(nbout / 10);
		ft_putchar(nbout % 10 + '0');
	}
}
