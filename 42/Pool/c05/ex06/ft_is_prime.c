/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/20 10:48:19 by areheis           #+#    #+#             */
/*   Updated: 2020/09/20 12:38:49 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int i;

	i = 2;
	if (nb <= 1)
		return (0);
	while (i * i <= nb && i < 46341)
	{
		if (i == nb)
			return (1);
		if (nb % i == 0)
		{
			return (0);
		}
		i++;
	}
	return (1);
}
/*
** ne se divise que par un ou lui meme
*/
