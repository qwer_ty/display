/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/17 13:14:10 by areheis           #+#    #+#             */
/*   Updated: 2020/09/20 16:23:05 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int i;
	int total;

	total = 1;
	i = 1;
	if (power == 0)
	{
		return (1);
	}
	if (power <= 0)
	{
		return (0);
	}
	while (i <= power)
	{
		total = total * nb;
		i++;
	}
	return (total);
}
