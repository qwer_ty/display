/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/17 09:08:29 by areheis           #+#    #+#             */
/*   Updated: 2020/09/20 16:19:45 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	long unsigned int res;

	res = 1;
	if (nb < 0)
	{
		return (!nb);
	}
	while (nb > 1)
	{
		res *= nb;
		nb--;
	}
	return (res);
}
