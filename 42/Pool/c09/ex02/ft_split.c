/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 19:07:27 by areheis           #+#    #+#             */
/*   Updated: 2020/09/29 13:11:15 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strdup(char *str, int size)
{
	char	*newstr;
	int		i;

	if (!(newstr = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	i = -1;
	while (++i < size)
		newstr[i] = str[i];
	newstr[i] = '\0';
	return (newstr);
}

int		ft_is_part_of_charset(char c, char *charset)
{
	while (*charset)
		if (c == *(charset++))
			return (1);
	return (0);
}

char	**ft_split(char *str, char *charset)
{
	char	**split;
	int		sep[ft_strlen(str) + 2];
	int		i;
	int		o;

	o = ft_strlen(str) + 2;
	i = 0;
	sep[0] = -1;
	if (!(split = malloc(sizeof(char*) * (o + 1))))
		return (NULL);
	i = -1;
	o = 0;
	while (str[++i])
		if (ft_is_part_of_charset(str[i], charset))
			sep[++o] = i;
	sep[++o] = ft_strlen(str);
	i = -1;
	o = -1;
	while (sep[++i] != ft_strlen(str))
		if (sep[i] != sep[i + 1] - 1)
			split[++o] = ft_strdup(&str[sep[i] + 1], sep[i + 1] - sep[i] - 1);
	split[++o] = NULL;
	return (split);
}
