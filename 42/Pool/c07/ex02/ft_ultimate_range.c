/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ulimate_range.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/22 13:42:33 by areheis           #+#    #+#             */
/*   Updated: 2020/09/23 17:12:29 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int count;

	i = 0;
	count = 0;
	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	(count) = max - min;
	(*range) = malloc(count * sizeof(int));
	if (!*range)
		return (-1);
	while (i < count)     
	{
		(*range)[i] = min + i;
		i++;
	}
	return (count);
}
