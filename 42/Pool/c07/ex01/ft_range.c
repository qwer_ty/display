/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/22 11:42:22 by areheis           #+#    #+#             */
/*   Updated: 2020/09/22 14:49:29 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int i;
	int count;
	int *range;

	count = max - min;
	i = 0;
	if (min >= max)
		return (NULL);
	range = malloc(count * sizeof(int));
	while (i < count)
	{
		range[i] = i + min;
		i++;
	}
	return (range);
}
