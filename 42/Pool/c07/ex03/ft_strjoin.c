/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/23 09:39:48 by areheis           #+#    #+#             */
/*   Updated: 2020/10/19 23:25:21 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
**  concatener toutes les strings pointees par strs
** utiliser sep comme separateur
** si la taille est 0, doit retourner un string vide
** compteur pour la taille de chaque string
** puis faire la somme de la taille de chaque string
*/

int		str_len(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

char	*ft_strcpy(char *dest, char *src)
{
	int i;

	i = -1;
	while (src[++i] != '\0')
	{
		dest[i] = src[i];
	}
	dest[i] = '\0';
	return (dest);
}

char	*insert_in_arr(char *arr, int size, char **strs, char *sep)
{
	int i;
	int j;

	j = 0;
	i = 0;
	while (i < size)
	{
		ft_strcpy(arr + j, strs[i]);
		j += str_len(strs[i]);
		if (i < size - 1)
		{
			ft_strcpy(arr + j, sep);
			j += str_len(sep);
		}
		i++;
	}
	return (arr);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		i;
	int		sum_leng;
	char	*arr;
	int		j;

	j = 0;
	i = 0;
	sum_leng = 0;
	while (i < size)
	{
		sum_leng += str_len(strs[i]);
		i++;
	}
	if (size > 0)
	{
		sum_leng += str_len(sep) * (size - 1);
	}
	arr = malloc((sum_leng + 1) * (sizeof(char)));
	if (!arr)
	{
		return (NULL);
	}
	insert_in_arr(arr, size, strs, sep);
	return (arr);
}
