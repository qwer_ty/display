/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/11 20:47:11 by areheis           #+#    #+#             */
/*   Updated: 2020/09/13 17:40:23 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{
	int i;
	int	j;
	int	min;
	int current;
	int tmp;

	i = -1;
	while (++i < size)
	{
		j = i;
		min = tab[j];
		current = j;
		while (j < size)
		{
			if (tab[j] < min)
			{
				min = tab[j];
				current = j;
			}
			j++;
		}
		tmp = tab[i];
		tab[i] = min;
		tab[current] = tmp;
	}
}
