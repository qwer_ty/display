void	ft_sort_int_tab(int *tab, int size);
#include <stdio.h>

int main()
{
	int	i;
	int arr[5] = {5, 2, 3, 4, 9};
	ft_sort_int_tab(arr, 5);
	i = -1;
	while (++i < 5)
		printf("%d, ", arr[i]);
	printf("\n");
	return(0);
}
