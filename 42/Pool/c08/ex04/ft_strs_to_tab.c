/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 15:44:47 by areheis           #+#    #+#             */
/*   Updated: 2020/09/28 18:07:10 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_str.h"
#include <stdlib.h>

/*
** Count the lenght of av
*/

int						ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		i++;
	}
	return (i);
}

/*
** malloc + copy av
*/

char					*ft_strcpy(char *str)
{
	char	*copy_str;
	int		i;

	i = 0;
	if (!(copy_str = malloc(sizeof(char) * (ft_strlen(str) + 1))))
	{
		return (NULL);
	}
	while (str[i])
	{
		copy_str[i] = str[i];
		i++;
	}
	copy_str[i] = '\0';
	return (copy_str);
}

struct	s_stock_str		*ft_strs_to_tab(int ac, char **av)
{
	int				i;
	t_stock_str		*n;

	i = 0;
	if (!(n = malloc(sizeof(t_stock_str) * (ac + 1))))
		return (NULL);
	while (i < ac)
	{
		n[i].size = ft_strlen(av[i]);
		n[i].str = av[i];
		if (!(n[i].copy = ft_strcpy(n[i].str)))
		{
			return (NULL);
		}
		i++;
	}
	n[i].size = 0;
	n[i].str = NULL;
	n[i].copy = NULL;
	return (n);
}
