/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 15:57:54 by areheis           #+#    #+#             */
/*   Updated: 2020/09/28 18:11:17 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stock_str.h"

void	print_str(char *str)
{
	while (*str)
		write(1, str++, 1);
}

void	print_number(int nb)
{
	char c;

	if (nb < 10)
	{
		c = '0' + nb;
		write(1, &c, 1);
	}
	else
	{
		print_number(nb / 10);
		c = '0' + (nb % 10);
		write(1, &c, 1);
	}
}

void	ft_show_tab(struct s_stock_str *n)
{
	int i;

	i = 0;
	while (n[i].str)
	{
		print_str(n[i].str);
		write(1, "\n", 1);
		print_number(n[i].size);
		write(1, "\n", 1);
		print_str(n[i].copy);
		write(1, "\n", 1);
		i++;
	}
}
