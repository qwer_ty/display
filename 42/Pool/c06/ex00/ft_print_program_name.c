/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_program_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 15:28:24 by areheis           #+#    #+#             */
/*   Updated: 2020/09/19 12:28:53 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	count(char *argv)
{
	int i;

	i = 0;
	while (*argv)
	{
		argv++;
		i++;
	}
	return (i);
}

int	main(int argc, char **argv)
{
	int i;

	i = count(*argv);
	(void)argc;
	write(1, argv[0], i);
	write(1, "\n", 1);
	return (0);
}
