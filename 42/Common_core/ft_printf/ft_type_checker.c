/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_checker.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 16:36:41 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:12:02 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	type_checker(char **format, t_flags *s_fl)
{
	if (**format == 'i' || **format == 'd')
		s_fl->pt += render_int(s_fl, va_arg(s_fl->ap, int));
	else if (**format == 'c')
		s_fl->pt = render_c(s_fl, va_arg(s_fl->ap, int));
	else if (**format == 's')
		s_fl->pt = render_s(s_fl, va_arg(s_fl->ap, char *));
	else if (**format == 'x')
		s_fl->pt += hex_render(s_fl, va_arg(s_fl->ap, unsigned int), 'x');
	else if (**format == 'X')
		s_fl->pt += hex_render(s_fl, va_arg(s_fl->ap, unsigned int), 'X');
	else if (**format == 'u')
		s_fl->pt += render_u(s_fl, va_arg(s_fl->ap, unsigned int));
	else if (**format == 'p')
		s_fl->pt = render_ptr(s_fl, va_arg(s_fl->ap, unsigned long long));
	else if (**format == '%')
		s_fl->pt = render_percent(s_fl);
	return (s_fl->pt);
}
