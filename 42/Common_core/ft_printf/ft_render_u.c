/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_render_u.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 13:15:31 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:13:10 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	render_u_width(t_flags *s_fl, int len)
{
	if (s_fl->preci < len)
		render_wi(s_fl->wi, len, s_fl);
	else
		render_wi(s_fl->wi, s_fl->preci, s_fl);
	return (0);
}

int	render_uleft(t_flags *s_fl, int len, unsigned int num)
{
	s_fl->fl = (s_fl->preci) ? 1 : 0;
	s_fl->pt += render_preci(s_fl->preci - len, s_fl);
	ft_putnbr(num, s_fl->pt);
	s_fl->fl = 0;
	return (render_u_width(s_fl, len));
}

int	render_u(t_flags *s_fl, unsigned int num)
{
	int len;

	s_fl->pt += lenght_int((long long)num);
	len = ft_lenght_base(num, 10);
	if (!num && s_fl->preci == 0)
	{
		s_fl->fl = 0;
		return (render_wi(s_fl->wi, 0, s_fl) - 1);
	}
	if (s_fl->fl == 2)
		return (render_uleft(s_fl, len, num));
	else if (s_fl->preci >= 0 || s_fl->fl == 1)
	{
		s_fl->fl = (s_fl->preci >= 0) ? 0 : 1;
		render_u_width(s_fl, len);
		s_fl->fl = 1;
		s_fl->pt += render_preci(s_fl->preci - len, s_fl);
	}
	else
		render_wi(s_fl->wi, len, s_fl);
	return (ft_putunbr(num, s_fl->pt));
}
