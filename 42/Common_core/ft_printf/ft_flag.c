/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flag.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/26 10:23:03 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:12:25 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_flag(char **format, t_flags *s_fl)
{
	while (**format == '-' || **format == '0')
	{
		if (**format == '-' && (s_fl->fl == 1 || s_fl->fl == 0))
			s_fl->fl = 2;
		else if (**format == '0' && s_fl->fl != 2)
			s_fl->fl = 1;
		(*format)++;
	}
	return (0);
}
