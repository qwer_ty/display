/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_width.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/28 09:52:23 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:13:18 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_width(char **format, t_flags *s_fl)
{
	if (ft_isdigit(**format))
	{
		s_fl->wi = ft_atoi(*format);
		*format += ft_lenght_base(s_fl->wi, 10);
	}
	else if (**format == '*')
	{
		s_fl->wi = va_arg(s_fl->ap, int);
		if (s_fl->wi < 0)
		{
			s_fl->wi *= -1;
			s_fl->fl = 2;
		}
		(*format)++;
	}
	return (0);
}
