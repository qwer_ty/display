/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_precision.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 12:42:06 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:12:30 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_precision(char **format, t_flags *s_fl)
{
	if (**format == '.')
	{
		s_fl->preci = 0;
		(*format)++;
		if (**format == '*')
		{
			s_fl->preci = va_arg(s_fl->ap, unsigned int);
			(*format)++;
		}
		else
		{
			while (ft_isdigit(**format))
			{
				s_fl->preci = ft_atoi(*format);
				if (s_fl->preci < 0)
					(*format)++;
				*format += ft_lenght_base(s_fl->preci, 10);
			}
		}
	}
	return (0);
}
