/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils_1.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 10:51:46 by areheis           #+#    #+#             */
/*   Updated: 2021/01/11 12:18:27 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_isdigit(int c)
{
	if (c <= '9' && c >= '0')
	{
		return (1);
	}
	return (0);
}

int		ft_atoi(const char *str)
{
	long int	res;
	int			sign;
	int			i;

	res = 0;
	sign = 1;
	i = 0;
	while (str[i] == ' ' || (str[i] >= '\t' && str[i] <= '\r'))
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			sign = -1;
		i++;
	}
	i--;
	while (str[++i] >= '0' && str[i] <= '9')
	{
		res = res * 10 + str[i] - '0';
	}
	return (sign * res);
}

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (1);
}

void	ft_bzero(void *str, size_t n)
{
	unsigned char *buff;

	buff = (unsigned char *)str;
	while (n-- > 0)
	{
		*(buff) = 0;
		buff++;
	}
}

void	*ft_calloc(size_t n, size_t size)
{
	void *dest;

	if (!(dest = malloc(n * size)))
		return (NULL);
	ft_bzero(dest, size * n);
	if (size == 0)
		return (dest);
	return (dest);
}
