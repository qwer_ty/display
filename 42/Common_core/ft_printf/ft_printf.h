/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 17:29:22 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:11:42 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stddef.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct		s_fl
{
	va_list			ap;

	int				pt;
	int				fl;
	int				preci;
	int				wi;
}					t_flags;

int					ft_flag(char **format, t_flags *s_fl);
int					ft_precision(char **format, t_flags *s_fl);
int					ft_isdigit(int c);
int					ft_atoi(const char *str);
int					ft_putchar(char c);
void				ft_bzero(void *str, size_t n);
void				*ft_calloc(size_t n, size_t size);
int					ft_putnbr(int n, int i);
unsigned long long	ft_lenght_base(unsigned long long value, unsigned int base);
int					ft_digit_lenght(long long i);
int					ft_putunbr(unsigned int n, int i);
int					print_filler(char **format, t_flags *s_fl);
void				init_flags(t_flags *s_fl);
int					ft_printf(const char *format, ...);
char				*hex_filler(char *h, unsigned long long n, char c, int he);
char				*hexconv(char *hex, unsigned long long n, char c, int hexa);
int					hex_printer(char *str);
int					hex_render(t_flags *s, unsigned long long n, char c);
int					render_hex_witdh(t_flags *s_fl, char *hex);
int					render_negative(int num, t_flags *s_fl);
int					render_zero(int num, t_flags *s_fl);
int					int_left(int num, t_flags *s_fl);
int					lenght_int(long long num);
int					render_int(t_flags *s_fl, int num);
int					render_preci(int precision, t_flags *s_fl);
int					render_wi(int witdh, int len, t_flags *s_fl);
int					render_c(t_flags *s_fl, char c);
int					render_percent(t_flags *s_fl);
int					render_ptr(t_flags *s_fl, unsigned long long num);
int					str_len(const char *str);
void				write_str(char const *s);
int					disp_str(char *str, t_flags *s_fl);
int					render_s(t_flags *s_fl, char *str);
int					render_u_width(t_flags *s_fl, int len);
int					render_uleft(t_flags *s_fl, int l, unsigned int n);
int					render_u(t_flags *s_fl, unsigned int num);
int					type_checker(char **format, t_flags *s_fl);
int					ft_width(char **format, t_flags *s_fl);
#endif
