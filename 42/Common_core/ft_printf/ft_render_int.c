/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_render_int.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 17:53:11 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:12:58 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	render_negative(int num, t_flags *s_fl)
{
	s_fl->wi--;
	ft_putchar('-');
	return (num *= -1);
}

int	render_zero(int num, t_flags *s_fl)
{
	if (num < 0 && s_fl->preci < 0)
		num = render_negative(num, s_fl);
	if (s_fl->wi > s_fl->preci)
	{
		if (s_fl->preci >= 0)
			s_fl->fl = 0;
		if (s_fl->preci < ft_digit_lenght(num))
			render_wi(s_fl->wi, ft_digit_lenght(num), s_fl);
		else
		{
			if (num < 0)
				s_fl->wi--;
			render_wi(s_fl->wi, s_fl->preci, s_fl);
		}
	}
	else
		render_wi(0, ft_digit_lenght(num), s_fl);
	if (num < 0)
		num = render_negative(num, s_fl);
	s_fl->preci = (s_fl->preci <= ft_digit_lenght(num))
	? 0 : s_fl->preci - ft_digit_lenght(num);
	s_fl->fl = 1;
	s_fl->pt += render_preci(s_fl->preci, s_fl);
	return (ft_putnbr(num, s_fl->pt));
}

int	int_left(int num, t_flags *s_fl)
{
	if (num < 0)
		num = render_negative(num, s_fl);
	if (s_fl->preci)
		s_fl->fl = 1;
	s_fl->pt += render_preci(s_fl->preci - ft_digit_lenght(num), s_fl);
	ft_putnbr(num, s_fl->pt);
	s_fl->fl = 0;
	if (s_fl->wi > s_fl->preci)
	{
		if (ft_digit_lenght(num) > s_fl->preci)
			s_fl->wi -= ft_digit_lenght(num);
		else
			s_fl->wi -= s_fl->preci;
	}
	else if (s_fl->wi <= s_fl->preci)
		s_fl->wi = 0;
	render_wi(s_fl->wi, 0, s_fl);
	return (0);
}

int	lenght_int(long long num)
{
	int i;

	i = 0;
	if (num <= 0)
		i++;
	while (num)
	{
		num = num / 10;
		i++;
	}
	return (i);
}

int	render_int(t_flags *s_fl, int num)
{
	s_fl->pt += lenght_int(num);
	if (!num && s_fl->preci == 0)
	{
		s_fl->fl = 0;
		return (render_wi(s_fl->wi, 0, s_fl) - 1);
	}
	if (num == -2147483648)
	{
		write_str("-2147483648");
		return (0);
	}
	if (s_fl->wi == s_fl->preci)
		s_fl->wi = 0;
	if (s_fl->fl == 2)
		return (int_left(num, s_fl));
	else if (s_fl->preci >= 0 || s_fl->fl == 1)
		return (render_zero(num, s_fl));
	else
		render_wi(s_fl->wi, ft_digit_lenght(num), s_fl);
	return (ft_putnbr(num, s_fl->pt));
}
