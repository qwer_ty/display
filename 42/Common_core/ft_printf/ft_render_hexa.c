/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_render_hexa.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 17:14:59 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:12:52 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*hex_filler(char *hex, unsigned long long n, char c, int hexa)
{
	int					tmp;
	unsigned long long	size;

	size = ft_lenght_base(n, 16) + 1;
	size += (hexa == 1) ? 2 : 0;
	hex[--size] = '\0';
	if (n == 0)
		hex[--size] = '0';
	while (n > 0)
	{
		tmp = n % 16;
		if (tmp < 10)
			hex[--size] = 48 + (tmp % 16);
		else
			hex[--size] = (c == 'x') ? 87 + (tmp % 16) : 55 + (tmp % 16);
		n /= 16;
	}
	if (hexa == 1)
	{
		hex[--size] = 'x';
		hex[--size] = '0';
	}
	return (hex);
}

char		*hexconv(char *hex, unsigned long long n, char c, int hexa)
{
	unsigned long long size;

	size = ft_lenght_base(n, 16) + 1;
	size += (hexa == 1) ? 2 : 0;
	if (!(hex = (char *)malloc(sizeof(char) * (size))))
		return (NULL);
	hex = hex_filler(hex, n, c, hexa);
	return (hex);
}

int			hex_printer(char *str)
{
	int i;

	i = 0;
	while (str && *str)
	{
		ft_putchar(*str);
		str++;
		i++;
	}
	return (i);
}

int			hex_render(t_flags *s_fl, unsigned long long num, char c)
{
	char *hex;

	hex = NULL;
	if (!num && s_fl->preci == 0)
	{
		s_fl->fl = 0;
		return (render_wi(s_fl->wi, 0, s_fl));
	}
	hex = (c == 'x') ? hexconv(hex, num, 'x', 0) : hexconv(hex, num, 'X', 0);
	render_hex_witdh(s_fl, hex);
	free(hex);
	return (0);
}

int			render_hex_witdh(t_flags *s_fl, char *hex)
{
	if (s_fl->fl == 2 || s_fl->wi < s_fl->preci)
	{
		s_fl->fl = (s_fl->preci >= 0) ? 1 : 0;
		s_fl->pt += render_preci(s_fl->preci - str_len(hex), s_fl);
		s_fl->pt += hex_printer(hex);
		s_fl->fl = 0;
		render_u_width(s_fl, str_len(hex));
	}
	else if (s_fl->fl == 1 || s_fl->preci >= 0)
	{
		s_fl->fl = (s_fl->preci >= 0) ? 0 : 1;
		render_u_width(s_fl, str_len(hex));
		s_fl->fl = (s_fl->preci >= 0) ? 1 : 0;
		s_fl->pt += render_preci(s_fl->preci - str_len(hex), s_fl);
		s_fl->pt += hex_printer(hex);
	}
	else
	{
		render_wi(s_fl->wi, str_len(hex), s_fl);
		s_fl->pt += hex_printer(hex);
	}
	return (s_fl->pt);
}
