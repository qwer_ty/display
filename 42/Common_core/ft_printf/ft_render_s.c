/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_render_s.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 11:48:24 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:13:05 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		str_len(const char *s)
{
	int c;

	c = 0;
	while (s[c] != '\0')
		c++;
	return (c);
}

void	write_str(char const *s)
{
	if (s == NULL)
		return ;
	while (*s)
	{
		write(1, s, 1);
		s++;
	}
}

int		disp_str(char *str, t_flags *s_fl)
{
	int i;
	int len;

	i = 0;
	len = s_fl->preci;
	while (*str && len--)
	{
		ft_putchar(*str);
		str++;
		i++;
	}
	return (i);
}

int		render_s(t_flags *s_fl, char *str)
{
	if (!str)
		str = "(null)";
	if (s_fl->preci >= str_len(str))
		s_fl->preci = str_len(str);
	if (s_fl->fl == 2)
		s_fl->pt += disp_str(str, s_fl);
	if (s_fl->preci >= 0)
		render_wi(s_fl->wi, s_fl->preci, s_fl);
	else
		render_wi(s_fl->wi, str_len(str), s_fl);
	if (s_fl->fl == 0 || s_fl->fl == 1)
		s_fl->pt += disp_str(str, s_fl);
	return (s_fl->pt);
}
