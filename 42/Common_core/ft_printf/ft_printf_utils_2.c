/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils_2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/11 11:10:51 by areheis           #+#    #+#             */
/*   Updated: 2021/01/15 16:42:20 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int					ft_putnbr(int n, int i)
{
	if (n == -2147483648)
		write(1, "-2147483648", 12);
	if ((n < 0))
	{
		n = n * (-1);
		ft_putchar('-');
	}
	if (n <= 9 && n >= 0)
		ft_putchar(n + 48);
	if (n > 9)
	{
		ft_putnbr(n / 10, i);
		ft_putchar((n % 10) + 48);
	}
	return (0);
}

unsigned long long	ft_lenght_base(unsigned long long value, unsigned int base)
{
	long long i;

	i = 1;
	if ((long long)value < 0)
		value -= 1;
	while (value >= base)
	{
		i++;
		value /= base;
	}
	return (i);
}

int					ft_digit_lenght(long long i)
{
	long long j;

	j = 0;
	if (i <= 0)
		j++;
	while (i != 0)
	{
		i /= 10;
		j++;
	}
	return (j);
}

int					ft_putunbr(unsigned int n, int i)
{
	if (n <= 9)
		ft_putchar(n + 48);
	if (n > 9)
	{
		ft_putnbr(n / 10, i);
		ft_putchar((n % 10) + 48);
	}
	return (0);
}
