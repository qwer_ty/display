/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 17:46:03 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:12:43 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		print_filler(char **format, t_flags *s_fl)
{
	(*format)++;
	ft_flag(format, s_fl);
	if (**format == ' ')
	{
		write(1, " ", 1);
		(*format)++;
	}
	ft_width(format, s_fl);
	ft_precision(format, s_fl);
	type_checker(format, s_fl);
	return (0);
}

void	init_flags(t_flags *s_fl)
{
	s_fl->preci = -1;
	s_fl->fl = 0;
	s_fl->wi = 0;
}

int		ft_printf(const char *format, ...)
{
	t_flags *s_fl;

	if (!(s_fl = ft_calloc(1, sizeof(t_flags))))
		return (0);
	va_start(s_fl->ap, format);
	while (*format)
	{
		if (*format == '%')
		{
			init_flags(s_fl);
			print_filler((char**)&format, s_fl);
		}
		else
			s_fl->pt += ft_putchar(*format);
		format++;
	}
	va_end(s_fl->ap);
	free(s_fl);
	return (s_fl->pt);
}
