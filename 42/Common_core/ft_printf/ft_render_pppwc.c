/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_render_pppwc.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/28 11:19:28 by areheis           #+#    #+#             */
/*   Updated: 2021/01/16 10:11:24 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	render_preci(int preci, t_flags *s_fl)
{
	int i;

	i = 0;
	while (preci-- > 0)
	{
		if (s_fl->fl == 1)
			ft_putchar('0');
		i++;
	}
	return (i);
}

int	render_wi(int width, int len, t_flags *s_fl)
{
	while ((width - len) > 0)
	{
		if (s_fl->fl == 1)
			ft_putchar('0');
		else
			ft_putchar(' ');
		width--;
		s_fl->pt++;
	}
	return (0);
}

int	render_c(t_flags *s_fl, char c)
{
	if (s_fl->fl == 2)
		ft_putchar(c);
	render_wi(s_fl->wi, 1, s_fl);
	if (s_fl->fl == 0)
		ft_putchar(c);
	return (s_fl->pt + 1);
}

int	render_percent(t_flags *s_fl)
{
	if (s_fl->fl == 2)
		s_fl->pt += ft_putchar('%');
	s_fl->pt += render_wi(s_fl->wi, 1, s_fl);
	if (s_fl->fl != 2)
		s_fl->pt += ft_putchar('%');
	return (s_fl->pt);
}

int	render_ptr(t_flags *s_fl, unsigned long long num)
{
	char *hex;

	hex = NULL;
	if (s_fl->preci == 0 && !num)
	{
		s_fl->pt += (s_fl->fl != 2) ? render_wi(s_fl->wi, 2, s_fl) : 0;
		write(1, "0x", 2);
		s_fl->pt += (s_fl->fl == 2) ? render_wi(s_fl->wi, 2, s_fl) : 0;
		return (s_fl->pt += 2);
	}
	else if (s_fl->preci > s_fl->wi)
	{
		write(1, "0x", 2);
		s_fl->fl = 1;
		hex = hexconv(hex, num, 'x', 0);
		render_hex_witdh(s_fl, hex);
		return (s_fl->pt += 2);
	}
	hex = hexconv(hex, num, 'x', 1);
	render_hex_witdh(s_fl, hex);
	free(hex);
	return (s_fl->pt);
}
