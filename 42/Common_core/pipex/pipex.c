/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/29 14:47:31 by areheis           #+#    #+#             */
/*   Updated: 2021/07/11 14:54:52 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

void	exec_first_cmd(t_data *data)
{
	dup2(data->infd, 0);
	dup2(data->fd[1], 1);
	close(data->fd[1]);
	close(data->fd[0]);
	execve(data->fullcmd1, data->argcmd1, data->env);
}

void	exec_second_cmd(t_data *data)
{
	dup2(data->outfd, 1);
	dup2(data->fd[0], 0);
	close(data->fd[0]);
	execve(data->fullcmd2, data->argcmd2, data->env);
}

void	pipex(t_data *data)
{
	int	pid;

	pid = fork();
	if (pid == -1)
		putstr("Fork() : error");
	else if (pid == 0)
		exec_first_cmd(data);
	close(data->fd[1]);
	pid = fork();
	if (pid == 0)
		exec_second_cmd(data);
	close(data->fd[0]);
	waitpid(data->fd[0], &(data->fd_status), 0);
	waitpid(data->fd[1], &(data->fd_status), 0);
}

void	check(t_data **data)
{
	if ((*data)->infd < 0)
		putstr("Infile descriptor error");
	if ((*data)->outfd < 0)
		 putstr("Outfile descriptor error");
	(*data)->argcmd1 = ft_split((*data)->fullcmd1, ' ');
	get_cmd_path(data, 1);
	(*data)->argcmd2 = ft_split((*data)->fullcmd2, ' ');
	get_cmd_path(data, 2);
}

int	main(int argc, char *argv[], char **env)
{
	t_data	*data;

	data = malloc(sizeof(t_data));
	if (!data)
		return (0);
	if (argc != 5)
	{
		putstr("Arguments error");
		return (0);
	}
	if (pipe(data->fd) != 0)
	{
		putstr("Pipe() error");
		return (0);
	}
	data->infd = open(argv[1], O_RDONLY);
	data->outfd = open(argv[4], O_WRONLY | O_CREAT | O_TRUNC, 0666);
	data->env = env;
	data->fullcmd1 = argv[2];
	data->fullcmd2 = argv[3];
	check(&data);
	pipex(data);
	close(data->infd);
	close(data->outfd);
	return (1);
}
