/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmds.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 13:22:44 by areheis           #+#    #+#             */
/*   Updated: 2021/07/11 14:34:27 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

char	*get_new_path(char **env)
{
	int		i;
	char	*new_path;

	i = 0;
	new_path = NULL;
	while (env[i])
	{
		if (!ft_strncmp("PATH=", env[i], 5))
		{
			new_path = ft_strdup(ft_strrchr(env[i], '=') + 1);
			break ;
		}
		i++;
	}
	return (new_path);
}

int	concatenate_paths(char *path, char **cmd_n, char *arg)
{
	char	*cmd;
	char	*tmp;

	cmd = ft_strjoin(path, "/");
	tmp = cmd;
	cmd = ft_strjoin(cmd, arg);
	free(tmp);
	if (open(cmd, O_RDONLY) > 0)
	{
		*cmd_n = ft_strdup(cmd);
		free(cmd);
		return (1);
	}
	free(cmd);
	return (0);
}

void	get_cmd_path(t_data **data, int cmd_n)
{
	int		i;
	char	*new_path;
	char	**split_path;
	int		check;

	check = 0;
	new_path = get_new_path((*data)->env);
	split_path = ft_split(new_path, ':');
	i = 0;
	while (split_path[i] && !check)
	{
		if (cmd_n == 1)
			check = concatenate_paths(split_path[i++], &(*data)->fullcmd1,
					(*data)->argcmd1[0]);
		else if (cmd_n == 2)
			check = concatenate_paths(split_path[i++], &(*data)->fullcmd2,
					(*data)->argcmd2[0]);
	}
	free(new_path);
	i = 0;
	while (split_path[i])
		free(split_path[i++]);
	free(split_path);
	if (!check)
		putstr("Error : command not found");
}
