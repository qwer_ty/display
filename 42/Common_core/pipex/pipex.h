/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/29 14:49:21 by areheis           #+#    #+#             */
/*   Updated: 2021/07/11 14:45:19 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H
# include <unistd.h>
# include <stdio.h>
# include <fcntl.h>
# include <stdlib.h>

typedef struct s_data
{
	int		infd;
	int		outfd;
	int		fd[2];
	int		fd_status;
	char	*fullcmd1;
	char	*fullcmd2;
	char	**argcmd1;
	char	**argcmd2;
	char	**env;
}	t_data;

/* PIPEX DATA UTILS*/
void	exec_first_cmd(t_data *data);
char	*get_new_path(char **env);
int		concatenate_paths(char *path, char **cmd_n, char *arg);
void	get_cmd_path(t_data **data, int cmd_n);
void	pipex(t_data *data);
void	check(t_data **data);

/* LIBFT UTILS */
void	putstr(char *str);
char	**ft_split(char const *str, char charset);
char	*ft_strdup(const char *src);
char	*ft_strjoin(char const *s1, char const *s2);
size_t	ft_strlen(const char *str);
int		ft_strncmp(const char *s1, const char *s2, size_t n);
char	*ft_strrchr(const char *s, int c);
#endif