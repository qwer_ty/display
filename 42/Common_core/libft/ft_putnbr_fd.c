/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 15:29:08 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 23:14:50 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long nbout;

	nbout = (long)n;
	if (nbout >= -9 && nbout <= -1)
	{
		ft_putchar_fd('-', fd);
		nbout = nbout * -1;
		ft_putchar_fd(nbout + '0', fd);
	}
	else if (nbout <= -10)
	{
		ft_putchar_fd('-', fd);
		nbout = nbout * -1;
		ft_putnbr_fd(nbout / 10, fd);
		ft_putchar_fd(nbout % 10 + '0', fd);
	}
	else if (nbout >= 0 && nbout <= 9)
	{
		ft_putchar_fd(nbout + '0', fd);
	}
	else if (nbout >= 10)
	{
		ft_putnbr_fd(nbout / 10, fd);
		ft_putchar_fd(nbout % 10 + '0', fd);
	}
}
