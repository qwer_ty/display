/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 11:54:15 by areheis           #+#    #+#             */
/*   Updated: 2020/11/21 11:31:50 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	isoverflow(unsigned long long res, int sign)
{
	if (sign == 1 && res >= 9223372036854775808ULL)
		return (-1);
	if (res >= 9223372036854775809ULL)
		return (0);
	return (1);
}

int	ft_atoi(const char *str)
{
	long int	res;
	int			sign;
	int			i;

	res = 0;
	sign = 1;
	i = 0;
	while (str[i] == ' ' || (str[i] >= '\t' && str[i] <= '\r'))
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			sign = -1;
		i++;
	}
	i--;
	while (str[++i] >= '0' && str[i] <= '9')
	{
		res = res * 10 + str[i] - '0';
		if (!isoverflow(res, sign) || isoverflow(res, sign) == -1)
			return (isoverflow(res, sign));
	}
	return (sign * res);
}
