/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 12:14:13 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 21:56:05 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	ft_bzero(void *str, size_t n)
{
	unsigned char *buff;

	buff = (unsigned char *)str;
	while (n-- > 0)
	{
		*(buff) = 0;
		buff++;
	}
}
