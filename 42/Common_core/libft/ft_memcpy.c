/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 13:06:54 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 22:18:34 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** destination size is same as source
** source = cpy a bloc of memory
** size = taille of this bloc
** return void *
*/

void	*ft_memcpy(void *destination, const void *source, size_t size)
{
	long unsigned int i;

	i = 0;
	if (destination == 0 && source == 0)
		return (0);
	while (i < size)
	{
		((char *)destination)[i] = ((char *)source)[i];
		i++;
	}
	return (destination);
}
