/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 16:52:19 by areheis           #+#    #+#             */
/*   Updated: 2020/11/21 11:23:45 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** alloue la mem pour n
** rempli cette mem de 0
** si n ou size == 0, return null ou pointeur qui pourra etre free avec succès
*/

void	*ft_calloc(size_t n, size_t size)
{
	void *dest;

	if (!(dest = malloc(n * size)))
	{
		return (NULL);
	}
	ft_bzero(dest, size * n);
	if (size == 0)
		return (dest);
	return (dest);
}
