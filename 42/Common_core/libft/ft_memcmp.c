/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 22:47:23 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 22:21:00 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *src1, const void *src2, size_t n)
{
	const unsigned char *p1;
	const unsigned char *p2;

	p1 = (unsigned char*)src1;
	p2 = (unsigned char*)src2;
	while (n--)
	{
		if (*p1 != *p2)
		{
			return (*p1 - *p2);
		}
		else
		{
			p1++;
			p2++;
		}
	}
	return (0);
}
