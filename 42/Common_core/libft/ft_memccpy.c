/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 16:45:33 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 22:18:01 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** copies bytes from src to dest,
** up to and including the first occurrence of the character c,
** or until cnt bytes have been copied, whichever comes first.
*/

void	*ft_memccpy(void *dest, const void *src, int c, size_t count)
{
	long unsigned int i;

	i = 0;
	while (count-- > 0)
	{
		((char *)dest)[i] = ((unsigned char *)src)[i];
		if (((unsigned char *)src)[i] == (unsigned char)c)
		{
			return (dest + i + 1);
		}
		i++;
	}
	return (NULL);
}
