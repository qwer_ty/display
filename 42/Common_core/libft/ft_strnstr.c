/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/02 10:33:50 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 23:00:42 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** locate first occur of null terminated string little in big
**	not more than len char are searched
** char that appears after a '\0' are not searched
** if little occurs nowhere in big, NULL is returned
** otherwise a pointer to the first character
** of the first occur of little is returned
*/

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	unsigned int	i;
	int				j;

	i = 0;
	if (!*little)
		return ((char*)big);
	while (big[i] != '\0' && i < len)
	{
		j = 0;
		while (big[i + j] == little[j] && (i + j) < len)
		{
			if (little[j + 1] == '\0')
			{
				return ((char *)&big[i]);
			}
			++j;
		}
		++i;
	}
	return (NULL);
}
