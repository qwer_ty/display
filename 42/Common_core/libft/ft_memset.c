/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: areheis <areheis@student.s19.be>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 12:14:38 by areheis           #+#    #+#             */
/*   Updated: 2020/11/14 21:55:24 by areheis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** fill n firsts octets of memory block
** pointed by str pointée par with value
** void *f(void *) = typecast
*/

void	*ft_memset(void *str, int value, size_t count)
{
	unsigned char *buff;

	buff = (unsigned char *)str;
	while (count-- > 0)
	{
		*(buff) = (unsigned char)value;
		buff++;
	}
	return (str);
}
